from lexer_rules import tokens
from expressions import *

def p_expression_plus(subexpr):
    'expression : expression expression PLUS'
    subexpr[0] = subexpr[1] + subexpr[2]

def p_expression_term(subexpr):
    'expression : term'
    subexpr[0] = subexpr[1]

def p_term_times(subexpr):
    'expression : expression expression TIMES'
    subexpr[0] = subexpr[1] * subexpr[2]

def p_term_factor(subexpr):
    'term : factor'
    subexpr[0] = subexpr[1]
def p_term_rest(subexpr):
	'expression : expression expression REST'
	subexpr[0] = subexpr[1] - subexpr[2]
def p_term_div(subexpr):
	'expression : expression expression DIV'
	subexpr[0]= subexpr[1] / subexpr[2]

def p_factor_num(subexpr):
    'factor : NUMBER'
    subexpr[0] = subexpr[1]

#def p_factor_expr(subexpr):
 #   'factor : LPAREN expression RPAREN'
  #  subexpr[0] = subexpr[2]

def p_error(subexpr):
    raise Exception("Syntax error.")
