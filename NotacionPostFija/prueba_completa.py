import lexer_rules
import parser_rules

from sys import stdin, stdout
from ply.lex import lex
from ply.yacc import yacc

#text = "14 6 + 2 *"

arr = stdin.read()
lineas = arr.split("\n")
lineas.pop(len(lineas)-1)
lexer = lex(module=lexer_rules)
parser = yacc(module=parser_rules)
for i in lineas:
	expression = parser.parse(i, lexer)
	print expression
"""
result = expression.evaluate()
print result
"""
