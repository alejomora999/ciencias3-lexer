from jinja2 import Template
import os
import pdfkit

def ambiente():
        os.system("mkdir Certificados")
        os.system("ls | grep '.temp' > archivos.txt")

ambiente()
template = Template("template.html")
arc = open('archivos.txt','r')
lineas = arc.read().split("\n")
lineas.pop()
for i in lineas:
        resultado = open('Certificados/'+i+'.html','w')
        plantilla = open(i,'r')
	archivo=plantilla.read().split("\n")
	for i in archivo:
		label = i.split(" : ")
		if (label[0]=='name'):
			name = label[1]
                elif (label[0]=='activity'):
			activity = label[1]
                elif (label[0]=='description'):
			description = label[1]
	usuario = {
		'name' : name,
		'course' : activity,
		'description' : description
	}
        html=template.render()
        resultado.write(html)
        resultado.close()
        pdfkit.from_string(resultado,'Certificados/'+i+'.pdf')
